Practica de Matriz de led 8x8

Con ayuda de arduino conectamos una matriz de led de 8x8 para poder visualizar un mensaje
este mensaje tiene es estatico

Para poder cambiar el mensaje se debe calcular el numero binario que representa las luces 
encendidas de cada columna pero como un entero y asi teniendo 8 columnas con 8 valores 
entero representativos de cada columna, ademas se debe de poner el tamaño del mensaje 
en la variable tamaño multiplicado por el numero de columnas, en el repositorio se uso 
7 por la razon de que una columna de la matriz de led estaba descompuesta.

Para el funcionamiento de la practica se uso un potenciometro para regular la velocidad
con que se pasaba el texto y un dip switch para poder pasar el texto en uno de los cuatro
modos los cuales son:
-letra por letra hacia la derecha
-letra por letra hacia la izquierda
-barrido del mensaje hacia la derecha
-barrido del mensaje hacia la izquierda

Cualquier otra inquitud puede observar el pdf de requisitos de la practica


