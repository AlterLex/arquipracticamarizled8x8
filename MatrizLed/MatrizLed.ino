int Filas [8];//Array de filas
int Columnas[8];//Array de columnas
int Velocidad;//Velocidad del movimineto 
int Estado1;//Movimiento o letra por letra
int Estado2;//Desplazamiento izq derecha
int pinEstado1=12;
int pinEstado2=11;
int Estado1A;//Estados anteriores
int Estado2A;//Estados anteriores
int Posicion=0;//Posicion del vector de mensajes

int Potencias[]={1,2,4,8,16,32,64,128,256};//Potencias de 2

int Mensaje[]={0,168,112,248,112,168,0,/***/
               0,128,128,255,128,128,0,/*T*/
               0,255,136,136,136,112,0,/*P*/
               0,33,65,255,1,1,0,/*1*/
               0,8,8,8,8,8,0,/*-*/
               0,255,129,141,137,207,0,/*G*/
               0,255,152,148,146,97,0,/*R*/
               0,254,1,1,1,254,0,/*U*/
               0,255,136,136,136,112,0,/*P*/
               0,126,129,129,129,126,0,/*0*/
               0,102,153,153,153,102,0,/*8*/
               0,8,8,8,8,8,0,/*-*/
               0,114,137,137,137,70,0,/*S*/
               0,255,137,137,137,137,0,/*E*/
               0,126,129,129,129,66,0,/*C*/
               0,126,129,129,129,66,0,/*C*/
               0,129,129,255,129,129,0,/*I*/
               0,126,129,129,129,126,0,/*0*/
               0,255,32,16,8,255,0,/*N*/
               0,0,0,0,0,0,0,/* */
               0,127,136,136,136,127,0,/*A*/
               0,168,112,248,112,168,0/***/
               };

int Tamano=22*7;/*Columnas de leds(7) Cantidad de letras*/

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
//DECLARACION DE PINES
  for(int i=0;i<8;i++){
    Filas[i]=i+2;
    pinMode(Filas[i],OUTPUT);
    digitalWrite(Filas[i],HIGH);
    Columnas[i]=30+(i*2);
    pinMode(Columnas[i],OUTPUT);
    digitalWrite(Columnas[i],LOW);
    }
    
  pinMode(pinEstado1,INPUT);
  pinMode(pinEstado2,INPUT);
  Estado1A=digitalRead(pinEstado1);
  Estado2A=digitalRead(pinEstado2);
  
}

void loop() {
  // put your main code here, to run repeatedly:

  //PONER VELOCIDAD EN TERMINOS DE UN RANGO DE VELOCIDAD
  Velocidad=map(analogRead(A0),0,1024,50,100);

  //RECOLECCION DE DATOS DE LOS DIPSWITCH
  Estado1=digitalRead(pinEstado1);
  Estado2=digitalRead(pinEstado2);

    if(Estado1!=Estado1A){
      Estado1A=Estado1;
      delay(20);
    }

    if(Estado2!=Estado2A){
      Estado2A=Estado2;
      delay(20);
    }


//IMPRESION DE LA LETRAS 
    int PosicionTemp=Posicion;
    for(int temp=0;temp<Velocidad;temp++){

      for(int j=7;j>0;j--){
      EncenderFila(Mensaje[PosicionTemp],Filas[j]);
      PosicionTemp++;
      if(PosicionTemp>=Tamano){
          PosicionTemp=0;
        }
      }
      PosicionTemp=Posicion;
    }


    //BLOQUE DE IF PARA TOMAS DECISIONES DEL MOVIMIENTO DEL TEXTO EN LOS LEDS  
      if(Estado1==0 && Estado2==0){
        //movimiento izq-der
        Posicion=Posicion+1;
        if(Posicion>=Tamano){
              Posicion=0;
            }
     }
      else if(Estado1==0 && Estado2==1){
        //movimiento der-izq 
        Posicion=Posicion-1;
        if(Posicion<=0){
              Posicion=Tamano-1;
            }   
        }
      else if(Estado1==1 && Estado2==0){
        //letra izq-der
        Posicion=Posicion+7;
        while(Posicion%7!=0){
            Posicion=Posicion+1;
          }
          if(Posicion>=Tamano){
              Posicion=0;
            }
        }
      else{
          //letra der-izq
          Posicion=Posicion-7;
          while(Posicion%7!=0){
            Posicion=Posicion-1;
          }
          if(Posicion<=0){
              Posicion=Tamano-7;
            }
        }

      
      



}


 //METODO PARA ENCENDER UN FILA
void EncenderFila(int valor, int fila){
  digitalWrite(fila,LOW);
  for(int i=0;i<8;i++){
      int EstadoPin=valor&Potencias[i];
      if(EstadoPin>0){
          digitalWrite(Columnas[i],HIGH);
        }
       else{
          digitalWrite(Columnas[i],LOW);
        }
    }

    delay(2);
    digitalWrite(fila,HIGH);
    for(int i=0;i<8;i++){
          digitalWrite(Columnas[i],LOW);
    }
  
  }



